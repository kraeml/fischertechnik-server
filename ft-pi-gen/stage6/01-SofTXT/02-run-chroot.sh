#!/bin/bash -eux

mkdir /home/"${FIRST_USER_NAME}"/bin/ || true
mkdir /home/"${FIRST_USER_NAME}"/workspace/ || true

echo "Install SofTXT"
su - -c "git clone https://github.com/harbaum/SofTXT.git /home/"${FIRST_USER_NAME}"/bin/SofTXT" "${FIRST_USER_NAME}"

echo "Getting IOServer"
mkdir -p /home/"${FIRST_USER_NAME}"/workspace/IOServer/{lib,src}
su - -c "cd /home/"${FIRST_USER_NAME}"/workspace && wget https://github.com/harbaum/ftduino/archive/refs/heads/master.zip && unzip master.zip" "${FIRST_USER_NAME}"
cp -lr /home/"${FIRST_USER_NAME}"/workspace/ftduino-master/ftduino/libraries/* /home/"${FIRST_USER_NAME}"/workspace/IOServer/lib
cp -lr /home/"${FIRST_USER_NAME}"/workspace/ftduino-master/ftduino/libraries/WebUSB/examples/IoServer/* /home/"${FIRST_USER_NAME}"/workspace/IOServer/src/

cat > /home/"${FIRST_USER_NAME}"/workspace/IOServer/platformio.ini <<EOF
; PlatformIO Project Configuration File
;
;   Build options: build flags, source filter
;   Upload options: custom upload port, speed and extra flags
;   Library options: dependencies, extra library storages
;   Advanced options: extra scripting
;
; Please visit documentation for the other options and examples
; https://docs.platformio.org/page/projectconf.html

[env:ftduino]
platform = atmelavr
board = ftduino
framework = arduino
lib_deps =
	adafruit/Adafruit GFX Library @ ^1.10.13
	adafruit/Adafruit BusIO @ ^1.10.3
	Wire
	SPI
EOF

chown --recursive "${FIRST_USER_NAME}":"${FIRST_USER_NAME}" /home/"${FIRST_USER_NAME}"/bin/
chown --recursive "${FIRST_USER_NAME}":"${FIRST_USER_NAME}" /home/"${FIRST_USER_NAME}"/workspace/

if [ ! -f /etc/udev/rules.d/99-ftduino.rules ]
then
  wget -P /etc/udev/rules.d https://raw.githubusercontent.com/harbaum/ftduino/master/ftduino/driver/99-ftduino.rules
fi

# Running in chroot, ignoring request: daemon-reload
# systemctl daemon-reload
# Will done via cloudinit
# systemctl enable jupyter.service
sed -i -E "s/#(\s*- 'systemctl enable softxt.service)/\1/g" /boot/user-data
sed -i -E "s/#(\s*- 'systemctl start softxt.service)/\1/g" /boot/user-data

wget -P /etc/udev/rules.d https://raw.githubusercontent.com/harbaum/ftduino/master/ftduino/driver/99-ftduino.rules
