#!/bin/bash -eux

sed -i -E "s/#(\s*- 'systemctl enable cloud9.service)/\1/g" /boot/user-data
sed -i -E "s/#(\s*- 'systemctl start cloud9.service)/\1/g" /boot/user-data
